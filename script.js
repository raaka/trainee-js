var atual 	 = -1;
var questoes = $.get('https://quiz-trainee.herokuapp.com/questions');
var total = 0;
var answers = [];

function mostrarQuestao() {

	//Verificar se os dados foram carregados da api
	if (questoes.readyState != 4) {
		alert('Carregando API ...');
		return false;
	}

	//Pegar a resposta
	if (atual != -1) {
		try{
			var answer = Number($('[name=resposta]:checked')[0].value);
			answers.push(answer);
	
		}catch(e){
			alert('Selecione a resposta');
			return false;
		}
	}

	//Atualizar contador de questão
	atual++;

	//Mudar nome de botao
	if (atual == 0) {
		$('#confirmar').text('Próxima');
    	$("#resultado").hide();
		$('#listaRespostas').show();
		$("#voltar").hide();
	}

	//Finalizar quiz
	if (atual >= questoes.responseJSON.length) {
		finalizarQuiz();
		return false;
	}else{

		if (atual == 1) {
			$("#voltar").show();
		}
		//Chama a questão
		exibirUnicaQuestao(questoes.responseJSON);
		
		// console.log(atual);
	}	

	
}

function exibirUnicaQuestao(questoes){

	//Atualizar o título
	$("#titulo").text(questoes[atual].title);
	
	//Formar as questões
	var options='';
	for (var i = 0; i < questoes[atual].options.length; i++) {
		options+='<li><input type="radio" name="resposta" value="'+questoes[atual].options[i].value+'"><span>'+questoes[atual].options[i].answer+'</span></li>';
	}

	//Colocar na ul
	$('#listaRespostas').html(options);
}

function voltarQuestao(){
	if (atual != 0) {
		atual--;
		exibirUnicaQuestao(questoes.responseJSON);
		answers = answers.slice(0, answers.length-1);
	}
}

function finalizarQuiz() {
    atual=-1;

    for (var i = 0; i < answers.length; i++) {
    	total+=answers[i];
    }

	total = Math.round((total / 15) * 100);

    $("#listaRespostas").hide();
    $("#voltar").hide();

	$("#titulo").text("Quiz dos Valores da GTi");
    $("#resultado").show();
    $("#resultado").text('Sua pontuação: ' + total + '%');
    
	$('#confirmar').text('Refazer Quiz');
	answers = [];
    total=0;
}